import flask.app

from .user import userBP


def initControllers(app: flask.app.Flask):
    app.register_blueprint(userBP)
