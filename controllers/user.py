import jwt
from flask import Blueprint, Flask, jsonify, make_response, request
from http import HTTPStatus
from models.user import User
from middlewares.auth import authMW

userBP = Blueprint('user-controller', __name__, url_prefix='/user')


@userBP.route("/", methods=['POST'])
def register():
    data = request.get_json()
    email = data.get('email')
    username = data.get('username')
    password = data.get('password')
    try:
        newUserID = User.create(username=username, email=email, password=password)
        return make_response({"id": str(newUserID)}, HTTPStatus.ACCEPTED)
    except Exception as e:
        return make_response({}, HTTPStatus.INTERNAL_SERVER_ERROR)


@userBP.route("/login", methods=['POST'])
def login():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    try:
        user = User.get(User.email == email, User.password == password)
        eId = jwt.encode({"id": str(user.id)}, "secret", algorithm="HS256")
        return make_response({"token": str(eId)}, HTTPStatus.OK)
    except Exception as e:
        return make_response({}, HTTPStatus.INTERNAL_SERVER_ERROR)


@userBP.route("/")
@authMW
def getUser(userId):
    print(userId)
    user = User.get(User.id == userId)
    return make_response({"username": user.username, "email": user.email}, HTTPStatus.OK)