from .base_model import BaseModel
from peewee import SqliteDatabase, Model, CharField, AutoField


class User(BaseModel):
    id = AutoField(primary_key=True)
    username = CharField()
    email = CharField(unique=True)
    password = CharField()
