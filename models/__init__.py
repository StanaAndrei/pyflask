from .base_model import db
from .user import User


def iniDB():
    db.connect()
    db.create_tables([User])
