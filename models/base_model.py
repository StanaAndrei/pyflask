from peewee import Model, SqliteDatabase

db = SqliteDatabase("db.db")


class BaseModel(Model):
    class Meta:
        database = db

