from flask import Flask, jsonify, make_response
from http import HTTPStatus
from controllers import initControllers
from models import iniDB

app = Flask(__name__)


@app.route("/")
def hello():
    return make_response(jsonify({"id": 12}), HTTPStatus.CREATED)


if __name__ == "__main__":
    iniDB()
    initControllers(app)
    app.run(port=5000)

